import imaplib
import re
import sys
import ConfigParser
import time
import os
pattern_uid = re.compile('\d+ \(UID (?P<uid>\d+)\)')


def create_imap_folder(imap, folder):
    target = ""
    for level in folder.split('/'):
        target += "{}/".format(level)
        imap.create(target)

def parse_uid(data):
    match = pattern_uid.match(data)
    return match.group('uid')


def move_message(imap, uid, folder):
    imap.uid('store', uid, '-FLAGS', '(\Seen)')
    result = imap.uid('COPY', uid, folder)
    if result[0] == 'OK':
        mov, data = imap.uid('STORE', uid, '+FLAGS', '(\Deleted)')
        imap.expunge()


def usage():
    print 'Usage: ' + sys.argv[0] + ' [h[10,12,14,16,18,20] | d[1-7] | w | m]'
    print '\th:   process hourly folder'
    print '\th10: process 10 hour folder'
    print '\th12: process 12 hour folder'
    print '\th14: process 14 hour folder'
    print '\th16: process 16 hour folder'
    print '\th18: process 18 hour folder'
    print '\th20: process 20 hour folder'
    print '\t'
    print '\td:   process daily folder'
    print '\td1:  process daily monday folder'
    print '\td2:  process daily tuesday folder'
    print '\td3:  process daily wednesday folder'
    print '\td4:  process daily thursday folder'
    print '\td5:  process daily friday folder'
    print '\td6:  process daily saturday folder'
    print '\td7:  process daily sunday folder'
    print '\t'
    print '\tw:   process weekly folder'
    print '\tm:   process monthly folder'


def process_folder(folder_config):
    try:
        imap_folder = Config.get('folders', folder_config)
    except ConfigParser.NoOptionError:
        print '[process_folder] Folder config %s not found' % folder_config
        return


    imap = imaplib.IMAP4_SSL(imap_server)
    imap.login(imap_login, imap_pass)
    status, err_message = imap.select(mailbox=imap_folder, readonly=False)
    if status!='OK':
        imap.logout()
        print("[process_folder] IMAP Folder '%s' not found" % imap_folder)
        return

    # create_imap_folder(imap, im_folder)

    result, data=imap.search(None, "All")
    email_ids = data[0].split()
    emails_uids = []
    for id in email_ids:
        resp, data = imap.fetch(id, "(UID)")
        emails_uids.append(parse_uid(data[0]))

    for uid in emails_uids:
        move_message(imap, uid, folder_inbox)

    imap.logout()



if __name__ == '__main__':

    # arguments
    param = 'a' # auto
    if len(sys.argv)>2:
        usage()
        sys.exit(1)
    elif len(sys.argv)==2:
        param = sys.argv[1]

    # config
    config_filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), "config.ini")
    Config = ConfigParser.ConfigParser()
    Config.read(config_filename)
    imap_server = Config.get('imap', 'server')
    imap_login = Config.get('imap', 'login')
    imap_pass = Config.get('imap', 'password')
    folder_inbox = Config.get('folders', 'inbox')

    lt = time.localtime()

    ################################################################################
    # HOURLY

    # every hour
    if (param=='a' and lt.tm_min==0) or param=='h':
        process_folder('hourly')

    # every day @ 10:00
    if (param=='a' and lt.tm_hour==10 and lt.tm_min==0) or param=='h10':
        process_folder('hourly_10h')

    # every day @ 12:00
    if (param=='a' and lt.tm_hour==12 and lt.tm_min==0) or param=='h12':
        process_folder('hourly_12h')

    # every day @ 14:00
    if (param=='a' and lt.tm_hour==14 and lt.tm_min==0) or param=='h14':
        process_folder('hourly_14h')

    # every day @ 16:00
    if (param=='a' and lt.tm_hour==16 and lt.tm_min==0) or param=='h16':
        process_folder('hourly_16h')

    # every day @ 18:00
    if (param=='a' and lt.tm_hour==18 and lt.tm_min==0) or param=='h18':
        process_folder('hourly_18h')

    # every day @ 20:00
    if (param=='a' and lt.tm_hour==20 and lt.tm_min==0) or param=='h20':
        process_folder('hourly_20h')

    ################################################################################
    # DAILY

    # every day @ 1:00 AM
    if (param=='a' and lt.tm_hour==1 and lt.tm_min==0) or param=='d':
        process_folder('daily')

    # every monday @ 1:10 AM
    if (param == 'a' and lt.tm_hour == 1 and lt.tm_min == 10 and lt.tm_wday == 0) or param == 'd1':
        process_folder('daily_monday')
    # every tuesday @ 1:10 AM
    if (param == 'a' and lt.tm_hour == 1 and lt.tm_min == 10 and lt.tm_wday == 1) or param == 'd2':
        process_folder('daily_tuesday')
    # every wednesday @ 1:10 AM
    if (param == 'a' and lt.tm_hour == 1 and lt.tm_min == 10 and lt.tm_wday == 2) or param == 'd3':
        process_folder('daily_wednesday')
    # every thursday @ 1:10 AM
    if (param == 'a' and lt.tm_hour == 1 and lt.tm_min == 10 and lt.tm_wday == 3) or param == 'd4':
        process_folder('daily_thursday')
    # every friday @ 1:10 AM
    if (param == 'a' and lt.tm_hour == 1 and lt.tm_min == 10 and lt.tm_wday == 4) or param == 'd5':
        process_folder('daily_friday')
    # every saturday @ 1:10 AM
    if (param == 'a' and lt.tm_hour == 1 and lt.tm_min == 10 and lt.tm_wday == 5) or param == 'd6':
        process_folder('daily_saturday')
    # every sunday @ 1:10 AM
    if (param == 'a' and lt.tm_hour == 1 and lt.tm_min == 10 and lt.tm_wday == 6) or param == 'd7':
        process_folder('daily_sunday')

    ################################################################################
    # WEEKLY

    # every monday @ 2:00 AM
    if (param=='a' and lt.tm_hour==2 and lt.tm_min==0 and lt.tm_wday==0) or param=='w':
        process_folder('weekly')

    ################################################################################
    # MONTHLY

    # every 1st day of month @ 3:00 AM
    if (param=='a' and lt.tm_hour==3 and lt.tm_min==0 and lt.tm_mday==1) or param=='m':
        process_folder('monthly')

