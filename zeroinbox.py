import logging
from optparse import OptionParser
import re
from datetime import datetime

from pythonzimbra.tools import auth
from pythonzimbra.request_xml import RequestXml
from pythonzimbra.response_xml import ResponseXml
from pythonzimbra.communication import Communication
import imaplib
from pprint import pprint

# memo: https://files.zimbra.com/docs/soap_api/8.8.15/api-reference/index.html

ZIMBRA_INBOX_ID = 2  # FIXME: ???


def recurse_print_folder(active_folder, level=0):
    if level:
        indent = ' '+'--'*level+' '
    else:
        indent = ''
    print "%s%s id=%s" % (indent, active_folder['absFolderPath'], active_folder['id'])
    if 'folder' in active_folder:
        if isinstance(active_folder['folder'],dict):
            recurse_print_folder(active_folder['folder'], level + 1)
        else:
            for f in active_folder['folder']:
                recurse_print_folder(f, level+1)


if __name__ == "__main__":
    server_name = 'zmail.bcisoft.fr'
    admin_account = 'christophe@bcisoft.fr'
    admin_password = '33e7c7c2f5c3f2b76ba034b5e5a4e424074af478da2f9d7d92e5b8f8622ffa41'
    server_url = "https://%s:7071/service/admin/soap" % server_name
    comm = Communication(server_url)
    token = auth.authenticate(server_url, admin_account, admin_password)
    #print "token=%s" % token

    folder_path = "/00 Inbox"
    get_folder_request = RequestXml()
    get_folder_request.set_auth_token(token)
    get_folder_request.add_request(
        "GetFolderRequest",
        {
            "folder": {
                "path": folder_path
            }
        },
        "urn:zimbraMail"
    )
    get_folder_response = ResponseXml()
    comm.send_request(get_folder_request, get_folder_response)
    if get_folder_response.is_fault():
        raise Exception(
            "Erreur getfolder "
            "key for domain %s: (%s) %s" % (
                folder_path,
                get_folder_response.get_fault_code(),
                get_folder_response.get_fault_message()
            )
        )
    folder = get_folder_response.get_response()["GetFolderResponse"]['folder']
    recurse_print_folder(folder)
    # imap = imaplib.IMAP4_SSL(server_name)
    # imap.login(admin_account, admin_password)
    # imap.select(mailbox='INBOX', readonly=True)
    # imap.logout()
    # print "IMAP OK"
